import subprocess,time,pywiwi

homeSSIDs = [
    "rooter50",
    "rooter25"
]

# Wifi adapter to track
targetGUID = "{B672429F-D8F6-4BC6-B479-D32C8A55C12D}"

# OpenVPN config file to use...You should run this script in the same
# directory as your config & certificates
ovpnCfg = "client1.ovpn"

p = None # OpenVPN Process (subprocess.Popen object)
lastSSID = None

def atHome():
    global lastSSID
    
    # Alternative to PiWiwi, however will open a new CMD window for each
    # call, sometimes...it basically doesn't run in the background very
    # nicely.
    #~ connectedSSID = subprocess.check_output(
        #~ "netsh wlan show interfaces"
    #~ ).split("Profile                : ")[1].split()[0].strip()

    ifaces = pywiwi.getWirelessInterfaces()
    for iface in ifaces:
        if iface.guid_string == targetGUID:
            connectedSSID = pywiwi.queryInterface(
                iface,"current_connection"
            )[0].wlanAssociationAttributes.dot11Ssid.SSID

    if connectedSSID != lastSSID:
        print "Network change detected from %s to %s" % (
            lastSSID, connectedSSID
        )
        lastSSID = connectedSSID

    if connectedSSID in homeSSIDs:
        return True
    return False

while True:
    try:
        home = atHome()
    except:
        continue

    if not home and not p:
        print "Starting VPN..."
        p = subprocess.Popen("openvpn --config %s" % ovpnCfg)
        print "VPN Started."
    elif home and p:
        p.kill()
        p = None
        print "VPN Terminated."

    time.sleep(1)
