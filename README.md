# Abandon
I no longer run Windows on any bare metal machines, thus this work is no longer relavant to me. I will leave the repo here for reference.

# DESCRIPTION #
A Python script for Windows 10 (probably works on older versions). Detects the currently connected SSID for a given Wifi GUID, and runs OpenVPN if you're NOT on a specified list of SSIDs ("home" SSIDs); else kills the VPN if you are on a listed "home" SSID. This is a personal script I wrote for myself, but thought the idea might be useful to others. The VPN in my case is literally running at my house, so when I'm at work or a friend's house, this script sits in the background and will auto-start my OpenVPN connection to home (or when I'm home, doesn't bother).

# REQUIREMENTS #
 - Windows 10 (probably works on 7 & 8; maybe Vista)
 - Python 2.7
 - PyWiwi: https://github.com/6e726d/PyWiWi

# USAGE #
This is a personal script. You will probably need to edit it quite a bit for your needs. I'll try to make it a bit more user-friendly in a coming release. You will need to decide also how to run it. I use Windows Task Scheduler to run it at log-in via pythonw.exe (note the W so no cmd window).

I have no advice on how to obtain your wlan GUID at this time. I used the facilities in the PyWiwi module.

#NOTE:
You will need to comment out line 376 in PyWiwi WindowsWifi.py per my bug submission here: https://github.com/6e726d/PyWiWi/issues/4